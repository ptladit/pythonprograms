"""

 A self-dividing number is a number that is divisible by every digit it contains.

For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.

Also, a self-dividing number is not allowed to contain the digit zero.

Given a lower and upper number bound, output a list of every possible self dividing number, including the bounds if possible. 

Approach:
1. Brute Force but without multiple functions
2. Approach from the solution includes definition of a function to check for self dividing, this also includes a separate definition of the function which uses divmod


"""

class Solution2(object):
    def selfDividingNumbers(self, left, right):
        def self_dividing(n):
            for d in str(n):
                if d == '0' or n % int(d) > 0:
                    return False
            return True
        """
        Alternate implementation of self_dividing:
        def self_dividing(n):
            x = n
            while x > 0:
                x, d = divmod(x, 10)
                if d == 0 or n % d > 0:
                    return False
            return True
        """
        ans = []
        for n in range(left, right + 1):
            if self_dividing(n):
                ans.append(n)
        return ans #Equals filter(self_dividing, range(left, right+1))


class Solution:
  def selfDividingNumbers(self, left, right):
    """
    :type left: int
    :type right: int
    :rtype: List[int]
    """
    
    ans = []
    
    i = left
    while (i <= right):
      flag1 = True
      for ch in str(i):
        if(int(ch) == 0):
          flag1 = False
          break
        elif (i % int(ch) != 0):
          flag1 = False
          break
      
      if (flag1 == True):
        ans.append(i)
      i += 1
      
    return(ans)


def main():
  newobj = Solution()
  
  OP = newobj.selfDividingNumbers(12, 22)
  print (OP)

if __name__ == '__main__':
  main()
