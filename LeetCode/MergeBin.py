"""

Given two binary trees and imagine that when you put one of them to cover the other, some nodes of the two trees are overlapped while the others are not.

You need to merge them into a new binary tree. The merge rule is that if two nodes overlap, then sum node values up as the new value of the merged node. Otherwise, the NOT null node will be used as the node of new tree. 

Approach:
1. Create a tree structure in python and define it's right and left elements
"""

class TreeNode(object):
  def __init__(self, x):
    self.val = x
    self.left = None
    self.right = None

class Solution:
  def mergeTrees(self, t1, t2):
    """
    :type t1: TreeNode
    :type t2: TreeNode
    :rtype: TreeNode
    """
    
    if not t1 and not t2:
      return None
    
    ans = TreeNode((t1.val if t1 else 0) + (t2.val if t2 else 0))
    ans.left = self.mergeTrees(t1 and t1.left, t2 and t2.left)
    """
    I am using t1 and t1.left as a shortcut for t1.left if t1 is not None else None.

    Here, “x and y” evaluates as follows: If x is truthy, then the expression evaluates as y. Otherwise, it evaluates as x.

    When t1 is None, then None is falsy, and t1 and t1.left evaluates as t1 which is None.

    When t1 is not None, then t1 is truthy, and t1 and t1.left evaluates as t1.left as desired.

    This is a standard type of idiom similar to the “?” operator in other languages. I want t1.left if t1 exists, otherwise nothing.

    Alternatively, I could use a more formal getattr operator: getattr(t1, 'left', None)
    """

    ans.right = self.mergeTrees(t1 and t1.right, t2 and t2.right)

    return (ans)

def main():
  newobj = Solution()
  
  tree1 = TreeNode(1)
  tree1.left = TreeNode(3)
  tree1.left.left = TreeNode(5)
  tree1.right = TreeNode(2)
  
  tree2 = TreeNode(2)
  tree2.left = TreeNode(1)
  tree2.left.right = TreeNode(4)
  tree2.right = TreeNode(3)
  tree2.right.right = TreeNode(7)
  
  #print (tree2.val)
  OP = newobj.mergeTrees(tree1, tree2)
  
  print (OP)
  print (OP.val)
  print (OP.left.val)

if __name__ == '__main__':
  main()
