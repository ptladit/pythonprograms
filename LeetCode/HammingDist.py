"""

The Hamming distance between two integers is the number of positions at which the corresponding bits are different.

Given two integers x and y, calculate the Hamming distance.

Approach:
1. Follow the straight up definition of Hamming distance

2. Solution from Leetcode. Further optimises the calculation of Hamming Distance

"""

## A Better solution from Leetcode comments
class Solution2(object):
    def hammingDistance(self, x, y):
        """
        :type x: int
        :type y: int
        :rtype: int
        """
        x = x ^ y
        y = 0
        while x:
            y += 1
            x = x & (x - 1)
        return y


class Solution:
  def hammingDistance(self, x, y):
    """
    :type x: int
    :type y: int
    :rtype: int
    """
    
    Count = 0
    
    binx = "{0:020b}".format(x)
    biny = "{0:020b}".format(y)
    
    #print(binx)
    #print(biny)
    
    if(len(binx) == len(biny)):
      for m,n in zip(binx, biny):
        if(m != n):
          Count += 1
    
    return(Count)
        
def main():
  newobj = Solution()
  OP = newobj.hammingDistance(1, 1)
  
  print(OP)

if __name__ == '__main__':
  main()
