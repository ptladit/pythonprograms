"""

Initially, there is a Robot at position (0, 0). Given a sequence of its moves, judge if this robot makes a circle, which means it moves back to the original place.

The move sequence is represented by a string. And each move is represent by a character. The valid robot moves are R (Right), L (Left), U (Up) and D (down). The output should be true or false representing whether the robot makes a circle. 

Approach:
1. Simulation: Imagine the robot on a coordinate system. Increment decrement the location according to the input moves

Time Complexity: O(N)

2. Build a dictionary with the input characters. Check if the calculations are equal as required

"""

# Better solution available on Leetcode
class Solution2(object):
  def judgeCircle(self, moves):
    x = y = 0
    for move in moves:
      if move == 'U': y -= 1
      elif move == 'D': y += 1
      elif move == 'L': x -= 1
      elif move == 'R': x += 1

  return x == y == 0



class Solution:
  def judgeCircle(self, moves):
    """
    :type moves: str
    :rtype: bool
    """
    
    #print ('The input is ' + moves) 

    allchars = dict()
    flag1 = 0
    flag2 = 0

    for char in moves:
      if char not in allchars:
        allchars[char] = 1
      else:
        allchars[char] += 1
    
    
    if 'R' in allchars or 'L' in allchars:
      flag1 += 1
      if 'R' in allchars and 'L' in allchars:
        if (allchars['R'] == allchars['L']):
          flag1 += 1 

    if 'U' in allchars or 'D' in allchars:
      flag2 += 1
      if 'U' in allchars and 'D' in allchars:
        if (allchars['U'] == allchars['D']):
          flag2+= 1
      

    if (flag1 == 2 and (flag2 == 2 or flag2 == 0)) or (flag2 == 2 and (flag1 == 2 or flag1 == 0)):
      return True
    else:
      return False


def main():
  newobj = Solution()
  OP = newobj.judgeCircle("LL")
  
  print(OP)

if __name__ == '__main__':
  main()
