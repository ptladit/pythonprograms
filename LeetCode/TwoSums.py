"""

Given an array of integers, return indices of the two numbers, such that they add up to a specific target

Approach: Brute-Force method
1. Use a dictionary to store the index of each value in numbers as encountered
2. Keep checking if the value already exists in the dictionary

Time Complexity: O(n^2

"""

def twoSum(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: List[int]
    """
    ans = {}
    
    for i in range(0, len(nums)):
        if target - nums[i] in ans:
            return (ans[target - nums[i]], i)
        else:
            ans[nums[i]] = i
    return 0
    
def main():
    nums = [1, 3, 7, 8]
    target = 9  
    ans = twoSum(nums, target)
    print(ans)

if __name__ == '__main__':
    main()


