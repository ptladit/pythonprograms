def repeat(s, exclaim = True):
	result = s + s + s # Can also use "s * 3" which is faster
	# "s * 3" is faster because * calculates the size of the resulting object once whereas with +, the calculation is made each time + is called
	# This is kind of a vectorized operation
	
	if exclaim:
		result = result + "!!"
	
	return result

if __name__ == "__main__":
	print (repeat('Yay'))
	print (repeat('Yay No Exclaimation here', False))
